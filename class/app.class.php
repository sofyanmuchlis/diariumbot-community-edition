<?php

class dgtl_diAriumTelkom extends dgtl_dbconn {

  var $ch = false;
  var $ip = '';
  var $last_html = '';
  var $logged_in = false;
  var $password = '';
  var $username = '';

  public function get_diArium_token($url) {
    $data  = file_get_html($url);
    $nodes = $data->find("input[type=hidden]");

    foreach ($nodes as $node) {
      $val = $node->value;

      return $val;
    }
  }

  private function session_expired() {
    $time = '06:00:00';
    $time = explode(':', $time);

    return ($time[0] * 60) + ($time[1]) + ($time[2] / 60);
  }


  public function addActivityLogs($userid, $acivity_nm, $activity_dtl = '') {

    $sql = "INSERT INTO dgtl_activity_logs SET 
                  id=UUID(),
                  userid='$userid',
                  activity_nm='$acivity_nm',
                  activity_dtl='$activity_dtl',
                  created_dt=NOW()";

    $this->connect()->query($sql);
  }

  public function checkLoginStatus($userid) {
    $sql   = "SELECT 
                  CAST(TIME_TO_SEC(TIMEDIFF(DATE_FORMAT(NOW(), '%H:%i'), 
                  DATE_FORMAT(created_dt, '%H:%i'))) / 60 AS UNSIGNED) AS time_login 
                FROM dgtl_activity_logs 
                WHERE userid='$userid' && 
                      activity_nm='LOGIN' && 
                      DATE(created_dt)=DATE(NOW)";
    $exec  = $this->connect()->query($sql);
    $count = $exec->num_rows;
    $data  = $exec->fetch_assoc();

    if ($count > 0) {
      if ($data['time_login'] <= $this->session_expired()) {
        return true;
      }

      return false;
    }

    return false;
  }

  public function do_login($token, $url, $ref = false) {
    $this->logged_in = false;

    $data = array(
        'username' => $this->username,
        'keypassword' => $this->password,
        '_token' => $token
    );

    $data = http_build_query($data);
    $res  = $this->dgtl_curl_post($url, $data);

    $this->last_html = $res['response'];

    if (preg_match('/^message/', $res['response'])) {
      trigger_error('CAN NOT LOGIN TO DIARIUM', E_USER_WARNING);

      return false;

    }

    $this->logged_in = true;

    $this->addActivityLogs($this->username, 'LOGIN');

    if (!$ref) {
      return $this->last_html;
    }

    return true;
  }

  public function auto_comment($id, $url) {

    $data = array(
        'val[type]' => 'post',
        'val[type_id]' => $id,
        'val[text]' => 'Tetap semangat guyss!!! #3M #TelkomSIgma'
    );

    $data = http_build_query($data);
    $this->dgtl_set_status($url, $data);
  }

  public function cleanRecognitionPost($url, $refid) {
    $this->dgtl_curl_get($url . $refid);
  }

  public function addRecognition($target_id, $comment_txt = '') {

    $data = array(
        'to_user_id' => $target_id,
        'recognition[0]' => 'Solid',
        'recognition[1]' => 'Speed',
        'recognition[2]' => 'Smart',
        'recognition[3]' => 'Imagine',
        'recognition[4]' => 'Focus',
        'recognition[5]' => 'Action',
        'recognition[6]' => 'Enthusiasm',
        'recognition[7]' => 'Integrity',
        'recognition[8]' => 'Totality',
        'comment' => $comment_txt
    );

    $data = http_build_query($data, $url);
    $this->dgtl_curl_post($url, $data);

  }

  public function auto_like($id, $url) {

    $url = $url . $id;

    $data = array(
        'emoji' => 1
    );

    $data = http_build_query($data);
    $this->dgtl_set_status($url, $data);
  }

  public function do_post($status, $url) {

    $data = array(
        'val[type]' => 'user-timeline',
        'val[type_id]' => '',
        'val[community_id]' => '',
        'val[link_title]' => '',
        'val[page_id]' => '',
        'val[content_type]' => 'text',
        'val[link_image]' => '',
        'val[the_link]' => '',
        'val[link_description]' => '',
        'image[]' => '',
        'val[privacy]' => 2,
        'video' => '',
        'share_file' => '',
        'val[text]' => "$status"
    );

    $data = http_build_query($data, $url);
    $this->dgtl_set_status($url, $data);
  }

  public function dgtl_curl_close() {

    if ($this->ch != false) {
      curl_close($this->ch);
    }

  }


  public function dgtl_curl_get($url, $ref = '') {

    if ($this->ch == false) {
      $this->dgtl_curl_init();
    }

    $ssl = false;

    if (preg_match('/^https/i', $url)) {
      $ssl = true;
    }

    if ($ssl) {
      curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);
    }

    if ($ref == '') {
      $ref = $url;
    }

    curl_setopt($this->ch, CURLOPT_URL, $url);
    curl_setopt($this->ch, CURLOPT_REFERER, $ref);
    $res  = curl_exec($this->ch);
    $info = curl_getinfo($this->ch);

    return array(
        'response' => trim($res),
        'info' => $info
    );
  }


  public function dgtl_curl_init() {
    $this->ch = curl_init();

    @curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
    @curl_setopt($this->ch, CURLOPT_AUTOREFERER, true);
    @curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, true);
    @curl_setopt($this->ch, CURLOPT_MAXREDIRS, 2);
    @curl_setopt($this->ch, CURLOPT_COOKIEFILE, dirname(__FILE__) . '/session/' . $this->username . '_cookie.txt');
    @curl_setopt($this->ch, CURLOPT_COOKIEJAR, dirname(__FILE__) . '/session/' . $this->username . '_cookie.txt');
    @curl_setopt($this->ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
  }

  public function dgtl_curl_post_mobile($url, $header, $post_data, $json = true, $ref = '') {

    if ($json) {
      $post_data = json_encode($post_data);
    }

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $result = curl_exec($ch);
    curl_close($ch);

    //print_r($result);

    return array('response' => trim($result));
  }


  public function dgtl_curl_post($url, $post_data, array $header, $ref = '') {

    if ($this->ch == false) {
      $this->dgtl_curl_init();
    }

    $ssl = false;

    if (preg_match('/^https/i', $url)) {
      $ssl = true;
    }

    if ($ssl) {
      curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);
    }

    if ($ref == '') {
      $ref = $url;
    }

    curl_setopt($this->ch, CURLOPT_URL, $url);
    curl_setopt($this->ch, CURLOPT_REFERER, $ref);
    curl_setopt($this->ch, CURLOPT_POST, 1);
    curl_setopt($this->ch, CURLOPT_POSTFIELDS, $post_data);

    if (!empty($header)) {
      curl_setopt($this->ch, CURLOPT_HTTPHEADER, $header);
    }

    $res  = curl_exec($this->ch);
    $info = curl_getinfo($this->ch);

    return array(
        'response' => trim($res),
        'info' => $info
    );
  }

  public function dgtl_set_status($url, $post_data, $ref = '') {

    if ($this->ch == false) {
      $this->dgtl_curl_init();

    }

    $ssl = false;

    if (preg_match('/^https/i', $url)) {
      $ssl = true;
    }

    if ($ssl) {
      curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);
    }

    if ($ref == '') {
      $ref = $url;
    }
    //$headers = array("Content-Type:multipart/form-data");

    curl_setopt($this->ch, CURLOPT_URL, $url);
    curl_setopt($this->ch, CURLOPT_REFERER, $ref);
    curl_setopt($this->ch, CURLOPT_POST, 1);
    curl_setopt($this->ch, CURLOPT_POSTFIELDS, $post_data);
    $res = curl_exec($this->ch);

    $info = curl_getinfo($this->ch);

    return array(
        'response' => trim($res),
        'info' => $info

    );
  }


  public function loadDraftPost($time) {
    $data = array();

    $exec = $this->connect()->query("SELECT * FROM dgtl_draftpost WHERE sch_post_tm='$time' AND off_sts='N'");
    while ($draft = $exec->fetch_assoc()) {
      $data[] = $draft;
    }

    if ($data) {

      return $data;
    }

    return false;
  }

  public function dayID() {
    $today   = date('Y-m-d');
    $day     = date('D', strtotime($today));
    $dayList = [
        'Sun' => 'Minggu',
        'Mon' => 'Senin',
        'Tue' => 'Selasa',
        'Wed' => 'Rabu',
        'Thu' => 'Kamis',
        'Fri' => 'Jumat',
        'Sat' => 'Sabtu',
    ];

    return $dayList [$day];
  }

  public function doLoginMobile($user, $pass, $url) {
    $this->logged_in = false;

    $header = array(
        "Accept: */*",
        "Content-Type: application/x-www-form-urlencoded",
        "X-Authorization: Basic bW9iYXBwXzc2MzQxOTgyNDpBZGJIRzZCbm4xMg=="
    );

    $post_data = array(
        'grant_type' => 'password',
        'username' => $user,
        'password' => $pass,
        'is_get' => true
    );

    $data = http_build_query($post_data);
    $res  = $this->dgtl_curl_post($url, $data, $header);

    return array('msg' => $res['response'], 'info' => $res['info']);
  }

  public function doCheckin() {

    $time_stamp = time();

    $header = array(
        "Content-Type: application/json",
        "Access-Control-Allow-Origin: *",
        "Access-Control-Allow-Methods: POST",
        "x-usr-id: 909057",
        "x-timestamp: 1542211439",
        "x-signature: GcxAFYa48iv9B/TUuEUrkCZDMOFEyZsXCIVcvf3CEqI=");

    $post_checkin = json_encode([
                                    'nik' => '895669',
                                    'feeling' => 'semangat',
                                    'ip' => '114.124.196.184',
                                    'catatan_checkin' => 'absentmasuk',
                                    'lang_checkin' => '-6.2951859', //'-6.222942300962646',
                                    'long_checkin' => '106.6634591',
                                    'posisi' => 'BCA KCU Serpong',
                                    'datetime' => '2018-11-15 10:11:01',
                                    'timezone' => 'wib',
                                    'kota' => 'Tangerang Selatan',
                                    'versi' => '3.8.0',
                                    'key' => 'MhXEyUuUDV282YRfgAbtR4KlET03y1', //'gKUs26kmi7TpdcRKwJHDtVn7TNuGDs',
                                ]);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, M_CHECKIN);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_checkin);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $result = curl_exec($ch);
    curl_close($ch);

    return array($result);
  }
}

?>